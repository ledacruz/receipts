package com.mba.receipts.messaging;

import com.google.gson.Gson;
import com.mba.receipts.model.Receipts;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ReceiptsListener {

    @Autowired
    ReceiptsProducer receiptsProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(ReceiptsListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-choreography}", groupId = "group-receipts")
    public void consumeChoreography(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Receipts receipts = convertStringToReceipts(message);

        switch (receipts.getEventType()) {
            case "accounts.sucess":
                producerMessage = "receipts.sucess";
                //producerMessage = "receipts.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "accounts.failure":
                producerMessage = "receipts.undo.sucess";
                //producerMessage = "receipts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "fraud.failure":
                producerMessage = "receipts.undo.sucess";
                //producerMessage = "receipts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "transferSender.failure":
                producerMessage = "receipts.undo.sucess";
                //producerMessage = "receipts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }
    }

    @KafkaListener(topics = "${topic-in}", groupId = "group-receipts")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));


        Receipts receipts = convertStringToReceipts(message);

        switch (receipts.getEventType()) {
            case "receipts.requested":
                producerMessage = "receipts.sucess";
                //producerMessage = "receipts.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
            case "undo.requested":
                producerMessage = "receipts.undo.sucess";
                //producerMessage = "receipts.undo.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        receiptsProducer.sendMessage(producerMessage, topic);
    }

    private Receipts convertStringToReceipts(String message){
        Gson gson = new Gson();
        Receipts receipts = gson.fromJson(message, Receipts.class);

        return receipts;

    }
}
