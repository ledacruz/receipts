package com.mba.receipts.controller;

import com.mba.receipts.messaging.ReceiptsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

    @RestController
    @RequestMapping(value = "/kafka")
    public class ReceiptsController {
        private final ReceiptsProducer receiptsProducer;

        @Autowired
        public ReceiptsController(ReceiptsProducer receiptsProducer) {
            this.receiptsProducer = receiptsProducer;
        }
        @PostMapping(value = "/publish")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.receiptsProducer.sendMessage(message, "");
        }
    }