package com.mba.receipts.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class Receipts {

    public String eventType;
    public String timestamp;
}
